# README #

prerelease Test

Webpack 5.x Configuration including Babel, Node-sass and postcss. With Optimization for Production Builds

### Configuration ###

- install module per npm
- remove old webpack Config (clear node_modules)
- copy webpack.config.js AND themes.json to your projects root folder
- configure themes.json according to your setup.
    - multiple themes for JS and CSS are possible
    - entry Path is relative to project Root
    - output is relative to webpack configs output.path
- check and modify config path in webpack config
    > const THEMES = require('./cfg/webpack/themes.json');
- update old webpack run scripts in package.json
    > "build": "NODE_ENV=production webpack --bail --progress",
    > "watch": "NODE_ENV=development webpack --watch --progress",
- enjoy

### TS ###

If TS is required, create a webpack config from webpack.ts-config.example.js in your root folder.

### Issues ###

- mini-css-extract-plugin
Dieses plugin extrahiert CSS aus dem JS (webpack buildet eigentlich nur js). Das Problem ist aktuell, dass die fast leeren JS Dateien aus dem das CSS extrahiert wurde nicht richtig gelöscht wird. Genauer funktioniert das "removeEmptyChunks" nicht wie es soll. Deswegen haben wir eine Notlösung gebaut, welche mit Hilfe eines Plugins (remove-files-webpack-plugin), die unnötigen Dateien löscht (https://github.com/webpack/webpack/issues/116719.
