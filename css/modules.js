const Path = require('path');
const fs = require('fs');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const Autoprefixer = require('autoprefixer');
const TailwindCss = require('tailwindcss');
const PostCSSLoaderPath = fs.existsSync(Path.resolve(__dirname,'../../../postcss-loader/')) ? Path.resolve(__dirname,'../../../postcss-loader/dist/index') :  Path.resolve(__dirname,'../node_modules/postcss-loader/dist/index');

/**
 * Exports the settings for css modules in webpack.config
 *
 * @param {string} ENV Environment to build for, expects 'production' for production and
 * anything else for non-production
 * @param {string} ROOT The root path of the application
 * @param {object} THEMES Object with Configuration for different Themes
 * @returns {{rules: [*,*,*,*]}}
 */
module.exports = (ENV, { ROOT, DIST}, THEMES) => {
  let cssAssetImagesConfig = [];
  let cssAssetFontsConfig = [];
  let scssConfiguration = [];

  // Check for base TW Config
  const twBaseConfig = {};

  if (fs.existsSync(Path.join(ROOT, 'tailwind.config.js'))) {
    Object.assign(twBaseConfig, require(Path.join(ROOT, 'tailwind.config.js')));
  }

  THEMES.scss.forEach(theme => {

    const themeName = theme.name;

    // check for theme TW Config
    const twThemeConfig = {};

    if (typeof theme.configs !== "undefined" && typeof theme.configs.tailwind !== "undefined") {
      Object.assign(twThemeConfig, require(Path.join(ROOT, theme.configs.tailwind)));
    }

    theme.paths.forEach(path => {
      const postCssPlugins = [];
      const resource = Path.join(ROOT, path.entry);
      const resourcePath = resource.substring(0, path.entry.lastIndexOf('/'));

      let twConfig = {};
      let twPathConfig = {};

      // check for Theme Path (colorset) tw config
      if (typeof path.configs !== "undefined" && typeof path.configs.tailwind !== "undefined") {
        twPathConfig = require(Path.join(ROOT, path.configs.tailwind));
      }

      // Build tw config
      twConfig = Object.assign({}, twBaseConfig, twThemeConfig, twPathConfig);
      // only apply tw if at least one config is found
      if (twConfig && Object.keys(twConfig).length > 0 && twConfig.constructor === Object) { // config still empty
        postCssPlugins.push(TailwindCss(twConfig));
      }

      // add theme based SCSS configuration
      const scssConfig = {
        resource:  resource,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: (ENV !== 'production'),
            },
          },
          {
            loader: PostCSSLoaderPath,
            options: {
              sourceMap: (ENV !== 'production'),
              postcssOptions: (loaderContext) => {
                return {
                  plugins: [
                    Autoprefixer(),
                    ...postCssPlugins
                  ]
                }
              }
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                includePaths: [resourcePath],
              }
            },
          }
        ]

      }
      scssConfiguration.push(scssConfig);
    });

    // add theme based asset configuration
    let assetSrc = 'htdocs/app/client/src/assets/themes'; // relative to Root, can be overwritten in themes.json
    let assetDist = 'themes'; // relative to htdocs, can be overwritten in themes.json

    if (typeof theme.assets !== "undefined") {
      if (typeof theme.assets.src !== "undefined") assetSrc = theme.assets.src;
      if (typeof theme.assets.dist !== "undefined") assetDist = theme.assets.dist;

      if (typeof theme.assets.mkdirDist == "undefined" || theme.assets.mkdirDist == true) {
        // Make sure image and webfonts dirs exist
        const assetDistImagePath = Path.join(Path.resolve(Path.join('htdocs',assetDist)), themeName, 'images');
        const assetDistFontPath = Path.join(Path.resolve(Path.join('htdocs',assetDist)), themeName, 'webfonts');

        if (!fs.existsSync(assetDistImagePath)){
            fs.mkdirSync(assetDistImagePath, { recursive: true });
        }

        if (!fs.existsSync(assetDistFontPath)){
            fs.mkdirSync(assetDistFontPath, { recursive: true });
        }
      }

    }

    const assetImageConfig = {
      include: Path.join(Path.resolve(assetSrc), themeName, 'images'), // only include theme folder in asset folder
      type: 'asset',
      generator: {
        filename: function (pathData) {
          // cut name
          let filepath = pathData.filename.substring(0, pathData.filename.lastIndexOf('/') + 1);
          // lets remove source directory
          filepath = filepath.replace(assetSrc, assetDist);
          // add HASH to name and add again
          return filepath + '[name][hash][ext][query][fragment]';
        }
      }
    }

    const assetFontConfig =  {
      include: Path.join(Path.resolve(assetSrc), themeName, 'webfonts'), // only include theme folder in asset folder
      type: 'asset/resource',
      generator: {
        filename: function (pathData) {
          // cut name
          let filepath = pathData.filename.substring(0, pathData.filename.lastIndexOf('/') + 1);
          // lets remove source directory
          filepath = filepath.replace(assetSrc, assetDist);
          // add Name again
          return filepath + '[name][ext][query][fragment]';
        }
      }
     }

    cssAssetImagesConfig.push(assetImageConfig);
    cssAssetFontsConfig.push(assetFontConfig);
  });


  return {
    rules: [
      {
        // Admin CSS needs special ruling , dont touch this if you dont know what you are doing
        test: /admin\/([\w_-]+)\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: (resourcePath, context) => {
                // publicPath is the relative path of the resource to the context
                return '../../../../';
              },
            }
          },
          {
            loader: 'css-loader',
            options: {
              import: true,
              url: true,
              sourceMap: (ENV !== 'production'),
            },
          },
          {
            loader: PostCSSLoaderPath,
            options: {
              sourceMap: (ENV !== 'production'),
              postcssOptions: {
                plugins: [
                  Autoprefixer()
                ]
              },

            },
          },
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                includePaths: [Path.join(ROOT, 'htdocs/app/client/src/styles/admin')],
              }
            },
          }
        ]
      },
      {
        test: /\.scss$/,
        exclude: /admin\/([\w_-]+)\.scss$/,
        oneOf: scssConfiguration
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              import: true,
              sourceMap: (ENV !== 'production'),
            },
          },
          {
            loader: PostCSSLoaderPath,
            options: {
              sourceMap: (ENV !== 'production'),
              plugins: [
                Autoprefixer()
              ],
            },
          },
        ]
      },
      {
        test: /\.(png|gif|jpg|svg)$/,
        oneOf: cssAssetImagesConfig
      },
      {
        test: /\.(woff2|woff|eot|otf|ttf|svg)$/,
        oneOf: cssAssetFontsConfig
      }
    ]
  };
};
