/* eslint-disable no-restricted-syntax */
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

function recursiveIssuer(m, c) {
  const issuer = c.moduleGraph.getIssuer(m);
  // For webpack@4 chunks = m.issuer

  if (issuer) {
    return recursiveIssuer(issuer, c);
  }

  const chunks = c.chunkGraph.getModuleChunks(m);
  // For webpack@4 chunks = m._chunks

  for (const chunk of chunks) {
    return chunk.name;
  }

  return false;
}

/**
 * Exports the settings for css optimization in webpack.config
 *
 * @param {string} ENV Environment to build for, expects 'production' for production and
 * anything else for non-production
 * @param {string} ROOT The root path of the application
 * @param {object} THEMES Object with Configuration for different Themes
 * @returns {{rules: [*,*,*,*]}}
 */
module.exports = (ENV, { ROOT }, THEMES) => {
  const cssCacheGroups = {};

  return {
    // removeEmptyChunks: true, // not working correctly atm https://github.com/webpack/webpack/issues/11671
    // splitChunks: {
    //   cacheGroups: cssCacheGroups
    // },
    minimize: (ENV === 'production'),
    minimizer: [
      new CssMinimizerPlugin(),
    ]
  };
};
