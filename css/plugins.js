const Path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FileManagerPlugin = require('filemanager-webpack-plugin');

/**
 * Exports the settings for css plugins in webpack.config
 *
 * @param {string} ENV Environment to build for, expects 'production' for production and
 * anything else for non-production
 * @param {string} ROOT The root path of the application
 * @param {object} THEMES Object with Configuration for different Themes
 * @returns {{rules: [*,*,*,*]}}
 */
module.exports = (ENV, { ROOT, DIST }, THEMES) => {
  let cssCleanWebpackPaths = [];

  THEMES.scss.forEach(theme => {
    theme.paths.forEach(path => {
      const themePath = path.output.substring(0, path.output.lastIndexOf('/') + 1);
      const cssCleanWebpackPath = Path.join(DIST, `${themePath}*`);
      cssCleanWebpackPaths.push(cssCleanWebpackPath);
    });

    cssCleanWebpackPaths = [...new Set(cssCleanWebpackPaths)];
  });

  let cssCleanWebpackPathsForFileManager = [];

  cssCleanWebpackPaths.forEach(path => {
    cssCleanWebpackPathsForFileManager.push({
      source: path,
      options: {
        force: true,
        recursive: true
      }
    });
  });

  return [
    new MiniCssExtractPlugin({
      filename: '[name].css'
    }),
    new FileManagerPlugin({
      events: {
        onStart: {
          delete: cssCleanWebpackPathsForFileManager
        }
      },
      runOnceInWatchMode: true
    }),
  ];
};
