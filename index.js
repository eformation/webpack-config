module.exports = {
  entryCSS: require('./css/entries'),
  moduleCSS: require('./css/modules'),
  optimizationCSS: require('./css/optimization'),
  pluginCSS: require('./css/plugins'),
  resolveCSS: require('./css/resolve'),


  entryJS: require('./js/entries'),
  moduleJS: require('./js/modules'),
  pluginJS: require('./js/plugins'),
  optimizationJS: require('./js/optimization'),
  resolveJS: require('./js/resolve'),
};
