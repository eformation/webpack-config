const Path = require('path');

/**
 * Exports the settings for javascript entries in webpack.config
 *
 * @param {string} ENV Environment to build for, expects 'production' for production and
 * anything else for non-production
 * @param {string} ROOT The root path of the application
 * @param {object} THEMES Object with Configuration for different Themes
 * @returns {{rules: Array.<*>}}
 */
module.exports = (ENV, { ROOT, DIST }, THEMES) => {
  const jsEntryPoints = {};

  THEMES.js.forEach(theme => {
    theme.paths.forEach(path => {
      jsEntryPoints[path.output] = Path.join(ROOT, path.entry);
    });
  });

  return jsEntryPoints;
};
