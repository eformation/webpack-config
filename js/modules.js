const Path = require('path');

/**
 * Exports the settings for javascript modules in webpack.config
 *
 * @param {string} ENV Environment to build for, expects 'production' for production and
 * anything else for non-production
 * @param {string} MODULES The modules folder
 * @param {string} THIRDPARTY A thirdparty folder, self-hosted modules.
 * @returns {{rules: Array.<*>}}
 */
module.exports = (ENV, { MODULES, THIRDPARTY, ROOT, DIST},  THEMES) => {

  let jsAssetImagesConfig = [];

  THEMES.js.forEach(theme => {

    const themeName = theme.name;

    let assetSrc = 'htdocs/app/client/src/assets/themes'; // relative to Root, can be overwritten in themes.json
    let assetDist = 'themes'; // relative to htdocs, can be overwritten in themes.json

    if (typeof theme.assets !== "undefined") {
      if (typeof theme.assets.src !== "undefined") assetSrc = theme.assets.src;
      if (typeof theme.assets.dist !== "undefined") assetDist = theme.assets.dist;
    }

    const assetImageConfig = {
      include: Path.join(Path.resolve(assetSrc), themeName, 'images'), // only include theme folder in asset folder
      type: 'asset',
      generator: {
        filename: function (pathData) {
          // cut name
          let filepath = pathData.filename.substring(0, pathData.filename.lastIndexOf('/') + 1);
          // lets remove source directory
          filepath = filepath.replace(assetSrc, assetDist);
          // add HASH to name and add again
          return filepath + '[name][hash][ext][query][fragment]';
        }
      }
    }

    jsAssetImagesConfig.push(assetImageConfig);

  });

  return {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: new RegExp(`(${MODULES}|${THIRDPARTY})`),
        use: ['babel-loader']
      },
      {
        test: /\.(png|gif|jpg)$/,
        oneOf: jsAssetImagesConfig
      },
    ]
  };
};
