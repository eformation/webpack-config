/* eslint-disable no-restricted-syntax */
const TerserPlugin = require('terser-webpack-plugin');

/**
 * Exports the settings for js optimization in webpack.config
 *
 * @param {string} ENV Environment to build for, expects 'production' for production and
 * anything else for non-production
 * @returns {{rules: [*,*,*,*]}}
 */
module.exports = (ENV) => {
  return {
    splitChunks: {
      chunks: 'async',
      minSize: 30000,
      minChunks: 1,
      maxAsyncRequests: 6,
      maxInitialRequests: 6,
      automaticNameDelimiter: '~',
      name: false,
      cacheGroups: {}
    },
    minimize: (ENV === 'production'),
    minimizer: [new TerserPlugin()],
  };
};
