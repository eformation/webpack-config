const Path = require('path');
const Webpack = require('webpack');
const FileManagerPlugin = require('filemanager-webpack-plugin');

/**
 * Exports the settings for js plugins in webpack.config
 *
 * @param {string} ENV Environment to build for, expects 'production' for production and
 * anything else for non-production
 * @param {string} ROOT The root path of the application
 * @param {object} THEMES Object with Configuration for different Themes
 * @returns {{rules: [*,*,*,*]}}
 */
module.exports = (ENV, { DIST }, THEMES) => {
  let jsCleanWebpackPaths = [];

  THEMES.js.forEach(theme => {
    theme.paths.forEach(path => {
      const themePath = path.output.substring(0, path.output.lastIndexOf('/') + 1);
      const jsCleanWebpackPath = Path.join(DIST, `${themePath}*`);

      jsCleanWebpackPaths.push(jsCleanWebpackPath);
    });

    jsCleanWebpackPaths = [...new Set(jsCleanWebpackPaths)];
  });

  let jsCleanWebpackPathsForFileManager = [];

  jsCleanWebpackPaths.forEach(path => {
    jsCleanWebpackPathsForFileManager.push({
      source: path,
      options: {
        force: true,
        recursive: true
      }
    });
  });

  return [
    new Webpack.ContextReplacementPlugin(
      /moment[\/\\]locale$/,
      /de|fr|en|nl|es|it|ar/
    ),
    new Webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(ENV),
      },
    }),
    new FileManagerPlugin({
      events: {
        onStart: {
          delete: jsCleanWebpackPathsForFileManager
        }
      },
      runOnceInWatchMode: true
    }),
  ];
};
