const Path = require("path");
const webpackConfig = require("@hajtech/webpack-config");

const THEMES = require("./cfg/webpack/themes.json");

const {
  entryCSS,
  moduleCSS,
  optimizationCSS,
  pluginCSS,
  resolveCSS,
  entryJS,
  moduleJS,
  pluginJS,
  optimizationJS,
  resolveJS,
} = webpackConfig;

const ENV = process.env.NODE_ENV;

const PATHS = {
  MODULES: "node_modules",
  ROOT: Path.resolve(),
  DIST: Path.resolve("htdocs"),
  PUBLIC_DIST: "/_resources/",
};

const config = [
  {
    name: "js",
    mode: ENV,
    entry: entryJS(ENV, PATHS, THEMES),
    output: {
      path: PATHS.DIST,
      publicPath: PATHS.PUBLIC_DIST,
      filename: "[name].js",
      chunkFilename: "[name]-[chunkhash].chunk.js",
    },
    devtool: ENV !== "production" ? "source-map" : false,
    plugins: pluginJS(ENV, PATHS, THEMES),
    module: {
      ...moduleJS(ENV, PATHS, THEMES),
      rules: [
        ...moduleJS(ENV, PATHS, THEMES).rules,
        {
          test: /\.tsx|\.ts?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
      ]
    }, optimization: optimizationJS(ENV, PATHS, THEMES),
    resolve: {
      ...resolveJS(ENV, PATHS, THEMES),
      extensions: [
        ...resolveJS(ENV, PATHS, THEMES).extensions,
        '.tsx',
        '.ts'
      ]
    }
  },
  {
    name: "css",
    mode: ENV,
    entry: entryCSS(ENV, PATHS, THEMES),
    output: {
      path: PATHS.DIST,
      publicPath: PATHS.PUBLIC_DIST,
    },
    devtool: ENV !== "production" ? "source-map" : false,
    module: moduleCSS(ENV, PATHS, THEMES),
    optimization: optimizationCSS(ENV, PATHS, THEMES),
    plugins: pluginCSS(ENV, PATHS, THEMES),
    resolve: resolveCSS(ENV, PATHS, THEMES)
  }
];

module.exports = process.env.WEBPACK_CHILD
  ? config.find((entry) => entry.name === process.env.WEBPACK_CHILD)
  : (module.exports = config);
